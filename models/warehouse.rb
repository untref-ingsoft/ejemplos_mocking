class Warehouse
  @products = {}

  def add(product_name, product_quantity)
    @products[product_name] = product_quantity
  end

  def remove(product_name, product_quantity)
    @products[product_name] -= product_quantity
  end

  def has_inventory?(product_name)
    get_inventory(product_name) > 0
  end

  def get_inventory(product_name)
    @products[product_name] || 0
  end
end

class Order
  attr_reader :product_name, :product_quantity

  def initialize(product_name, product_quantity)
    @product_name = product_name
    @product_quantity = product_quantity
    @filled = false
  end

  def fill(warehouse)
    if warehouse.has_inventory?(product_name, product_quantity)
      warehouse.remove(product_name, product_quantity)
      true
    else
      false
    end
  end

  def is_filled?
    @filled
  end
end
